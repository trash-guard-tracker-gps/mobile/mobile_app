import AsyncStorage from '@react-native-async-storage/async-storage';
import {useIsFocused, useNavigation} from '@react-navigation/native';
import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  ImageBackground,
  Image,
  Alert,
} from 'react-native';
import {BACKEND_URL} from '@env';
import {PickUpOrderStatus} from '../enum';

const Home = () => {
  const navigation = useNavigation();
  const isFocused = useIsFocused();
  const [user, setUser] = useState(null);
  const [status, setStatus] = useState('');
  const [statusButton, setStatusButton] = useState(false);

  useEffect(() => {
    (async () => {
      try {
        const userData = await AsyncStorage.getItem('user');
        if (userData !== null) {
          const parsedUserData = JSON.parse(userData);
          setStatus(parsedUserData.status);
          setUser(parsedUserData);

          const response = await axios.post(
            `${BACKEND_URL}/api/customer/get-by-id/${parsedUserData.id}`,
          );

          const data_customer = response.data.result[0];

          if (data_customer && parsedUserData !== null) {
            const updatedUserData = {
              ...parsedUserData,
              status: status || data_customer.order_status,
              cust_lat: data_customer.cust_lat || parsedUserData.cust_lat,
              cust_long: data_customer.cust_long || parsedUserData.cust_long,
              officer_lat:
                data_customer.officer_lat || parsedUserData.officer_lat,
              officer_long:
                data_customer.officer_long || parsedUserData.officer_long,
            };
            await AsyncStorage.setItem('user', JSON.stringify(updatedUserData));
            setStatus(data_customer.order_status);
          }
        }
      } catch (err) {
        console.log('Error retrieving user data:', err);
        Alert.alert('Error', err.response.data.error);
      }
    })();
  }, [statusButton, isFocused]);

  const handleConfirmAccepted = async () => {
    console.log('masuk');
    try {
      const response = await axios.post(
        `${BACKEND_URL}/api/order/update/${Number(user.orderId)}`,
        {
          status: '4',
        },
      );

      console.log('Update successful', response);

      const userData = await AsyncStorage.getItem('user');
      if (userData !== null) {
        const currentUserData = JSON.parse(userData);
        const updatedUserData = {
          ...currentUserData,
          officer_name: '',
          officer_lat: 0,
          officer_long: 0,
          status: '4',
        };
        await AsyncStorage.setItem('user', JSON.stringify(updatedUserData));
        setStatus('4');
      }
    } catch (err) {
      console.log('Error updating information:', err);
      Alert.alert('Error', err.response.data.error);
    } finally {
      setStatusButton(!statusButton);
    }
  };

  const displayStatus = () => {
    if (status == PickUpOrderStatus.Process) {
      return (
        <View
          style={[
            styles.requestContainer,
            {
              width: '100%',
              height: 40,
              backgroundColor: '#F5ECCE',
              borderColor: '#B79831',
              color: 'black' + '!important',
              flexDirection: 'row',
              marginTop: 15,
            },
          ]}>
          <Image source={require('../assets/images/process.png')} />
          <Text style={styles.requestText}>Request in process</Text>
        </View>
      );
    } else if (status == PickUpOrderStatus.Approved) {
      return (
        <View
          style={[
            styles.requestContainer,
            {
              height: '40',
              width: '100%',
              backgroundColor: '#E3F6E0',
              borderColor: '#43D62C',
              color: 'black',
              marginTop: 15,
              padding: 20,
            },
          ]}>
          <View
            style={{
              marginBottom: 10,
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Image
              style={{color: 'white'}}
              source={require('../assets/images/check.png')}
            />

            <Text style={[styles.requestText]}>Request has been approved</Text>
          </View>

          <View style={styles.hr}></View>
          <Text style={{color: 'black'}}>
            Wait for further notification until we appoint an officer to pick up
            your trash.
          </Text>
        </View>
      );
    } else if (status == PickUpOrderStatus.OnTheWay) {
      return (
        <View
          style={[
            styles.requestContainer,
            {
              height: '40',
              width: '100%',
              backgroundColor: '#E3F6E0',
              borderColor: '#43D62C',
              color: 'black',
              marginTop: 15,
              padding: 20,
            },
          ]}>
          <View
            style={{
              marginBottom: 10,
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Image
              style={{color: 'white'}}
              source={require('../assets/images/check.png')}
            />

            <Text style={[styles.requestText, {marginLeft: 5}]}>
              Officer on the way
            </Text>
          </View>
          <View style={styles.hr}></View>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('MapTracker', {refresh: true});
            }}
            style={{backgroundColor: '#3A9106', padding: 10}}>
            <Text style={{color: 'white'}}>Track</Text>
          </TouchableOpacity>
        </View>
      );
    } else if (status == PickUpOrderStatus.Arrived) {
      return (
        <View
          style={[
            styles.requestContainer,
            {
              height: '40',
              width: '100%',
              backgroundColor: '#E3F6E0',
              borderColor: '#43D62C',
              color: 'black',
              marginTop: 15,
              padding: 20,
            },
          ]}>
          <View
            style={{
              marginBottom: 10,
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Image
              style={{color: 'white'}}
              source={require('../assets/images/check.png')}
            />

            <Text style={[styles.requestText, {marginLeft: 5}]}>Finish</Text>
          </View>
          <View style={styles.hr}></View>
          <TouchableOpacity
            onPress={() => handleConfirmAccepted()}
            style={{backgroundColor: '#3A9106', padding: 10}}>
            <Text style={{color: 'white'}}>Okay</Text>
          </TouchableOpacity>
        </View>
      );
    } else {
      return (
        <View
          style={[
            styles.requestContainer,
            {
              width: '80%',
              height: 40,
              backgroundColor: 'lightblue',
              borderColor: 'darkblue',
              flexDirection: 'row',
              marginTop: 15,
            },
          ]}>
          <Image source={require('../assets/images/request.png')} />
          <Text style={styles.requestText}>There are no requests yet</Text>
        </View>
      );
    }
  };

  return (
    <>
      <ImageBackground
        style={styles.container_header}
        source={require('../assets/images/green.png')}>
        <View style={styles.header}>
          <View style={{maxWidth: '50%'}}>
            <Text style={styles.headerText}>Hi,</Text>
            <Text style={styles.parentText}>
              Let's start keeping the environment clean!
            </Text>
          </View>
          <Image
            style={{height: 80, width: 180, margin: 15}}
            source={require('../assets/images/truck_bg.png')}
          />
        </View>
      </ImageBackground>

      <View style={styles.background}>
        <View style={styles.container}>
          <View style={styles.bodyContainer}>
            <View style={styles.buttonsContainer}>
              <TouchableOpacity
                style={[styles.button, {height: 60}]}
                onPress={() => {
                  if (status == 5 || status == 4) {
                    navigation.navigate('FormSchedule', {refresh: true});
                  }
                }}>
                <Image
                  style={{
                    tintColor: '#3A9106',
                  }}
                  source={require('../assets/images/truck.png')}
                />
                <Text style={{marginTop: 10, color: 'black'}}>Trash</Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={[styles.button, {height: 60}]}
                onPress={() => {
                  navigation.navigate('MapTracker', {refresh: true});
                }}>
                <Image
                  style={{tintColor: '#3A9106'}}
                  source={require('../assets/images/location.png')}
                />
                <Text style={{marginTop: 10, color: 'black'}}>
                  Track Officer
                </Text>
              </TouchableOpacity>
            </View>

            <View style={{marginBottom: 10, width: '100%'}}>
              <Text
                style={{
                  color: 'black',
                  textAlign: 'left',
                }}>
                Trash pick up status
              </Text>
            </View>

            <View style={styles.hr}></View>

            {displayStatus()}

            <View style={styles.pickUpContainer}>
              <View style={{maxWidth: '65%'}}>
                <Text style={styles.greenText}>Pick me up,</Text>
                <Text style={styles.darkGreenText}>
                  Say goodbye to piles of trash welcome a cleaner and greener
                  world with CSOW! Make it easy to set real-time pickup
                  schedules by tracking your trash eco-friendly.
                </Text>
              </View>
              <Image
                style={{height: 70, width: 120}}
                source={require('../assets/images/pick.png')}
              />
            </View>
          </View>
        </View>

        <View
          style={[
            styles.bottomBar,
            // {marginTop: status == 0 || status == 4 || status == 5 ? 100 : 10},
            {
              marginTop: [
                PickUpOrderStatus.Process,
                PickUpOrderStatus.None,
                5,
              ].includes(status)
                ? 100
                : 10,
            },
          ]}>
          <TouchableOpacity
            onPress={() => navigation.navigate('Home', {refresh: true})}
            style={[styles.barItem]}>
            <Image
              style={{color: 'white', padding: 1, tintColor: '#3A9106'}}
              source={require('../assets/images/home.png')}
            />
            <Text style={{color: '#3A9106'}}>Home</Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => {
              if ([PickUpOrderStatus.None, 5].includes(status)) {
                navigation.navigate('FormSchedule', {refresh: true});
              }
            }}
            style={styles.barItem}>
            <Image
              style={{color: 'white', padding: 1}}
              source={require('../assets/images/truck.png')}
            />
            <Text style={{color: 'grey'}}>Pickup Menu</Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => {
              navigation.navigate('MapTracker', {refresh: true});
            }}
            style={styles.barItem}>
            <Image
              style={{color: 'white', padding: 1}}
              source={require('../assets/images/location.png')}
            />
            <Text style={{color: 'grey'}}>Track</Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => navigation.navigate('Detail', {refresh: true})}
            style={styles.barItem}>
            <Image
              style={{color: 'white', padding: 1}}
              source={require('../assets/images/profile.png')}
            />
            <Text style={{color: 'grey'}}>Account</Text>
          </TouchableOpacity>
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  background: {
    padding: 20,
    resizeMode: 'cover',
  },
  container_header: {
    height: 200,
    overflow: 'hidden',
    borderBottomLeftRadius: 50,
    borderBottomRightRadius: 50,
  },
  headerText: {
    color: '#3A9106',
    fontSize: 25,
    fontWeight: 'bold',
  },
  header: {
    flex: 1,
    fontWeight: 'bold',
    marginTop: 50,
    justifyContent: 'center',
    flexDirection: 'row', // Arrange children horizontally
    marginLeft: 30,
  },
  parentText: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'white',
    marginTop: 8,
  },
  bodyContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '80%',
    marginBottom: 20,
  },
  button: {
    flex: 1,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    margin: 5,
  },
  hr: {
    borderBottomColor: 'black',
    borderBottomWidth: 1,
    width: '100%',
    marginBottom: 10,
  },
  requestContainer: {
    width: '80%',
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    borderWidth: 1,
    marginBottom: 10,
  },
  requestText: {
    color: 'black',
    marginLeft: 10,
  },
  pickUpContainer: {
    flexDirection: 'row', // Arrange children horizontally
    backgroundColor: '#3A9106',
    padding: 15,
    marginTop: 30,
    width: '100%',
    borderRadius: 5,
    alignItems: 'center', // Align children vertically
  },
  greenText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 16,
  },
  darkGreenText: {
    paddingTop: 5,
    color: 'white',
  },
  bottomBar: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    paddingVertical: 10,
    margin: 10,
  },
  barItem: {
    alignItems: 'center',
  },
});

export default Home;
