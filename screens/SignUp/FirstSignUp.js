import React from 'react';
import {View, Text, TouchableOpacity, StyleSheet, Image} from 'react-native';
import {useNavigation} from '@react-navigation/native';

const FirstSignUp = () => {
  const navigation = useNavigation();

  return (
    <View style={styles.background}>
      <View style={styles.container}>
        <View style={styles.container_images}>
          <Image
            source={require('../../assets/images/logo.png')}
            // style={styles.logo}
            style={{marginTop: 25}}
          />

          <Image
            source={require('../../assets/images/earth.png')}
            style={{marginTop: 40}}
          />
        </View>

        <View style={styles.container_header}>
          <Text style={styles.headerText}>The Planet Wiser</Text>
          <Text style={styles.paragraphText}>Think before you trash it</Text>
        </View>

        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('FormSignUp')}>
          <Text style={styles.buttonText}>Create Account</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => navigation.navigate('Login')}>
          <View style={styles.container_login}>
            <Text style={{color: 'black'}}>You have account?</Text>
            <Text style={styles.signupText}>Login</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  background: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
  container: {
    flex: 1,
    backgroundColor: 'rgba(255, 255, 255, 0.8)',
    padding: 20,
  },
  container_header: {
    marginBottom: 30,
    alignContent: 'center',
    textAlignVertical: 'center',
    alignItems: 'center',
  },
  container_images: {
    alignItems: 'center',
    marginBottom: 20,
  },
  headerText: {
    fontSize: 24,
    fontWeight: 'bold',
    textAlign: 'center',
    color: 'black',
  },
  paragraphText: {
    textAlign: 'center',
    color: 'grey',
    fontSize: 14,
  },
  container_login: {
    marginTop: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  signupText: {
    color: 'blue',
    marginLeft: 5,
  },
  button: {
    width: '100%',
    backgroundColor: '#3A9106',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 120,
    height: 50,
    borderRadius: 10,
  },
  buttonText: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
  },
});

export default FirstSignUp;
