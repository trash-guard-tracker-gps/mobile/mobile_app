import React, {useState} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Image,
  Alert,
  ScrollView,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import axios from 'axios';
import {BACKEND_URL} from '@env';

const PasswordInput = ({value, onChangeText}) => {
  const [secureTextEntry, setSecureTextEntry] = useState(true);

  const toggleSecureTextEntry = () => {
    setSecureTextEntry(!secureTextEntry);
  };

  return (
    <View style={styles.passwordInputContainer}>
      <TextInput
        style={styles.input_password}
        placeholder="Password"
        placeholderTextColor="grey"
        value={value}
        onChangeText={onChangeText}
        secureTextEntry={secureTextEntry}
      />
      <TouchableOpacity onPress={toggleSecureTextEntry}>
        <Image
          source={require('../../assets/images/eye.png')}
          style={styles.eyeIcon}
        />
      </TouchableOpacity>
    </View>
  );
};

const FormSignUp = () => {
  const navigation = useNavigation();
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [address, setAddress] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');

  const handleRegister = async () => {
    if (password !== confirmPassword) {
      Alert.alert('Error', 'Password not same with confirm password');
      return;
    }

    const emailRegex = /\S+@\S+\.\S+/;

    if (!emailRegex.test(email)) {
      Alert.alert('Error', 'Please enter a valid email address with use @');
      return;
    }

    console.log('password', password);

    try {
      await axios.post(`${BACKEND_URL}/api/auth/register`, {
        name,
        phone: phoneNumber,
        password,
        email,
        address,
      });

      setName('');
      setPhoneNumber('');
      setPassword('');
      setEmail('');
      setAddress('');
      navigation.navigate('Login');
    } catch (err) {
      console.error('Registration failed:', err);
      Alert.alert('Error', err.response.data.error);
    }
  };

  return (
    <ScrollView contentContainerStyle={styles.background}>
      <View style={styles.navigationBar}>
        <TouchableOpacity
          style={{marginLeft: 10}}
          onPress={() => navigation.goBack()}>
          <Image source={require('../../assets/images/arrow.png')} />
        </TouchableOpacity>
        <Text style={{color: 'white', fontWeight: 'bold', marginLeft: 10}}>
          Register Account
        </Text>
      </View>
      <View style={styles.container}>
        <Text
          style={{
            textAlign: 'center',
            marginBottom: 20,
            color: 'black',
            fontSize: 15,
            fontWeight: 'bold',
          }}>
          Please fill in the form below to register an account
        </Text>
        <View style={styles.formContainer}>
          <Text style={{color: 'black', marginBottom: 5}}>Name</Text>
          <TextInput
            style={styles.input}
            placeholder="Fill your name"
            placeholderTextColor="grey"
            value={name}
            onChangeText={setName}
          />

          <Text style={{color: 'black', marginBottom: 5}}>Phone Number</Text>
          <TextInput
            style={styles.input}
            placeholder="Fill your phone number"
            placeholderTextColor="grey"
            value={phoneNumber}
            onChangeText={text => setPhoneNumber(text.replace(/[^0-9]/g, ''))}
            keyboardType="phone-pad"
            autoCompleteType="tel"
          />

          <Text style={{color: 'black', marginBottom: 5}}>Email</Text>
          <TextInput
            style={styles.input}
            placeholder="Fill your email"
            placeholderTextColor="grey"
            value={email}
            onChangeText={setEmail}
          />

          <Text style={{color: 'black', marginBottom: 5}}>Address</Text>
          <TextInput
            style={[styles.input, {height: 100}]}
            placeholder="Fill your address"
            placeholderTextColor="grey"
            value={address}
            onChangeText={setAddress}
            multiline={true}
          />

          <Text style={{color: 'black', marginBottom: 5}}>Password</Text>
          <PasswordInput value={password} onChangeText={setPassword} />

          <Text style={{color: 'black', marginBottom: 5}}>
            Confirm Password
          </Text>
          <PasswordInput
            value={confirmPassword}
            onChangeText={setConfirmPassword}
          />

          <TouchableOpacity style={styles.button} onPress={handleRegister}>
            <Text style={styles.buttonText}>Register</Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  background: {
    flexGrow: 1,
    backgroundColor: '#FFF',
    paddingBottom: 20,
  },
  input: {
    width: '100%',
    height: 40,
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 5,
    paddingHorizontal: 10,
    marginBottom: 20,
    color: 'black',
  },
  input_password: {
    flex: 1,
    height: 40,
    paddingHorizontal: 10,
    color: 'black',
  },
  container: {
    flex: 1,
    padding: 20,
  },
  passwordInputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 5,
    marginBottom: 20,
  },
  eyeIcon: {
    marginRight: 10,
    height: 20,
    width: 20,
  },
  navigationBar: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
    backgroundColor: '#3A9106',
  },
  formContainer: {
    justifyContent: 'center',
  },
  button: {
    width: '100%',
    marginTop: 10,
    borderRadius: 10,
    height: 50,
    backgroundColor: '#3A9106',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
  },
});

export default FormSignUp;
