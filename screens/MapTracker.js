import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Image,
  Linking,
  Alert,
} from 'react-native';
import {useIsFocused, useNavigation} from '@react-navigation/native';
import Geolocation from '@react-native-community/geolocation';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {BACKEND_URL} from '@env';
import axios from 'axios';

import {REVERB_APP_KEY, REVERB_HOST, REVERB_PORT, REVERB_SCHEME} from '@env';
import Echo from 'laravel-echo';
import Pusher from 'pusher-js/react-native';

Pusher.logToConsole = true;

let PusherClient = new Pusher(REVERB_APP_KEY, {
  cluster: 'mt1',
  wsHost: REVERB_HOST,
  wsPort: REVERB_PORT,
  enabledTransports: ['ws'],
  forceTLS: false,
});

let echo = new Echo({
  broadcaster: 'pusher',
  client: PusherClient,
});

const MapTracker = () => {
  const navigation = useNavigation();
  const isFocused = useIsFocused();
  const [permissionError, setPermissionError] = useState(null);

  const [currentAddress, setCurrentAddress] = useState('');
  const [address, setAddress] = useState('');
  const [targetAddress, setTargetAddress] = useState('');
  const [userData, setUserData] = useState(null);

  const [customerLat, setCustomerLat] = useState(0);
  const [customerLong, setCustomerLong] = useState(0);

  const [customerLocation, setCustomerLocation] = useState({
    lat: 0,
    long: 0,
  });
  const [userLocation, setUserLocation] = useState({
    lat: 0,
    long: 0,
  });

  const [id, setId] = useState(0);

  useEffect(() => {
    const fetchUserData = async () => {
      try {
        const userData = await AsyncStorage.getItem('user');
        console.log('userData', userData);
        if (userData !== null) {
          const parsedUserData = JSON.parse(userData);
          // setCustomerLat(parsedUserData.cust_lat);
          // setCustomerLong(parsedUserData.cust_long);

          setId(parsedUserData.id);
        }
      } catch (err) {
        console.log('Error retrieving user data:', err);
        Alert.alert(err.response.data.error);
      }
    };

    fetchUserData();

    // setInterval(() => {

    // }, 200);
  }, [isFocused]);

  useEffect(() => {
    Geolocation.requestAuthorization();
    const watchId = Geolocation.watchPosition(
      position => {
        console.log('position', position);
        setPermissionError(null);
        console.log('customerLat', customerLat, 'customerLong', customerLong);

        setCustomerLat(position.coords.latitude);
        setCustomerLong(position.coords.longitude);

        getCurrentAddress(position.coords.latitude, position.coords.longitude);

        echo.channel('user-location').listen('UserLocation', e => {
          // getAddress(+e.userLat, +e.userLong);
         currentDataAddress(
            {
              latitude: +e.userLat,
              longitude: +e.longitude,
            },
            setAddress,
          );

          console.log("mblunk", mblunk);
        });
      },
      error => {
        console.error('Geolocation error:', error.code, error.message);
        setPermissionError(
          'Location permission not granted or unable to determine location.',
        );
      },
      {enableHighAccuracy: true, timeout: 15000, maximumAge: 10000},
    );

    return () => Geolocation.clearWatch(watchId);
  }, [customerLat, customerLong]);

  useEffect(() => {
    const fetchDataAndUpdateLocation = async () => {
      try {
        const response = await axios.get(`${BACKEND_URL}/api/user/get`);
        const userData = response.data;
        setUserData(userData);

        if (userData && userData.latitude && userData.longitude) {
          await getTargetAddress(userData.latitude, userData.longitude);
          // await currentDataAddress(
          //   {
          //     latitude: userData.latitude,
          //     longitude: userData.longitude,
          //   },
          //   setTargetAddress,
          // );
        }
      } catch (error) {
        console.error('Error fetching user data:', error);
      }
    };

    const intervalId = setInterval(fetchDataAndUpdateLocation, 3000);

    fetchDataAndUpdateLocation();

    return () => clearInterval(intervalId);
  }, [id]);

  const getCurrentAddress = async (latitude, longitude) => {
    try {
      const response = await fetch(
        `https://nominatim.openstreetmap.org/reverse?format=json&lat=${latitude}&lon=${longitude}&addressdetails=1`,
      );
      const data = await response.json();
      const {address} = data;

      setCurrentAddress(
        `Street: ${address?.road ?? '-'}, City: ${
          address?.city ?? '-'
        }, Country: ${address?.country ?? '-'}. Region: ${
          address?.region ?? '-'
        }, Suburb: ${address?.suburb ?? '-'}, District: ${
          address?.district ?? '-'
        }, Postcode: ${address?.postcode ?? '-'}`,
      );
    } catch (error) {
      console.error('Error fetching address:', error);
    }
  };

  const currentDataAddress = async (
    {latitude, longitude},
    setCurrentDataAddress,
  ) => {
    try {
      const response = await fetch(
        `https://nominatim.openstreetmap.org/reverse?format=json&lat=${latitude}&lon=${longitude}&addressdetails=1`,
      );
      const data = await response.json();

      const {address} = data;

      setCurrentDataAddress(
        `Street: ${address?.road ?? '-'}, City: ${
          address?.city ?? '-'
        }, Country: ${address?.country ?? '-'}. Region: ${
          address?.region ?? '-'
        }, Suburb: ${address?.suburb ?? '-'}, District: ${
          address?.district ?? '-'
        }, Postcode: ${address?.postcode ?? '-'}`,
      );

      return "bedeh";
    } catch (error) {
      console.error('Error fetching address:', error);
    }
  };

  const getAddress = async (latitude, longitude) => {
    try {
      console.log('lat', typeof latitude, latitude);
      console.log('long', typeof longitude, longitude);

      const response = await fetch(
        `https://nominatim.openstreetmap.org/reverse?format=json&lat=${latitude}&lon=${longitude}&addressdetails=1`,
      );
      const data = await response.json();

      const {address} = data;

      setAddress(
        `Street: ${address?.road ?? '-'}, City: ${
          address?.city ?? '-'
        }, Country: ${address?.country ?? '-'}. Region: ${
          address?.region ?? '-'
        }, Suburb: ${address?.suburb ?? '-'}, District: ${
          address?.district ?? '-'
        }, Postcode: ${address?.postcode ?? '-'}`,
      );
    } catch (error) {
      console.error('Error fetching address:', error);
    }
  };

  const getTargetAddress = async (latitude, longitude) => {
    try {
      const response = await fetch(
        `https://nominatim.openstreetmap.org/reverse?format=json&lat=${latitude}&lon=${longitude}&addressdetails=1`,
      );
      const data = await response.json();
      const {address} = data;

      setTargetAddress(
        `Street: ${address?.road ?? '-'}, City: ${
          address?.city ?? '-'
        }, Country: ${address?.country ?? '-'}. Region: ${
          address?.region ?? '-'
        }, Suburb: ${address?.suburb ?? '-'}, District: ${
          address?.district ?? '-'
        }, Postcode: ${address?.postcode ?? '-'}`,
      );

      return `${address?.road}, ${address?.city}, ${address?.country}`;
    } catch (error) {
      console.error('Error fetching target address:', error);
    }
  };

  const handleOpenGoogleMaps = () => {
    if (
      customerLat &&
      customerLong &&
      userData &&
      userData.latitude &&
      userData.longitude
    ) {
      const url = `https://www.google.com/maps/dir/?api=1&origin=${customerLat},${customerLong}&destination=${userData.latitude},${userData.longitude}`;
      Linking.openURL(url);
    }
  };

  return (
    <View style={styles.background}>
      <View style={styles.navigationBar}>
        <TouchableOpacity
          style={{marginLeft: 10}}
          onPress={() => navigation.goBack()}>
          <Image source={require('../assets/images/arrow.png')} />
        </TouchableOpacity>
        <Text style={{color: 'white', fontWeight: 'bold', marginLeft: 10}}>
          Tracking picking up
        </Text>
      </View>
      <View style={styles.container}>
        {permissionError ? (
          <Text style={styles.errorText}>{permissionError}</Text>
        ) : (
          <View>
            <Text style={{color: 'grey', textAlign: 'center'}}>
              Your current location:
            </Text>
            <Text style={{color: 'black', textAlign: 'center'}}>
              {currentAddress}
            </Text>

            <View
              style={{
                borderBottomColor: 'black',
                borderBottomWidth: 0.5,
                marginVertical: 10,
              }}
            />

            <Text style={{color: 'grey', textAlign: 'center'}}>
              Your mark pickup trash location:
            </Text>
            <Text style={{color: 'black', textAlign: 'center'}}>{address}</Text>

            <View
              style={{
                borderBottomColor: 'black',
                borderBottomWidth: 0.5,
                marginVertical: 10,
              }}
            />

            <Text style={{color: 'grey', marginTop: 5, textAlign: 'center'}}>
              Name:
            </Text>
            <Text style={{color: 'black', textAlign: 'center'}}>Officer</Text>
            <Text style={{color: 'grey', marginTop: 15, textAlign: 'center'}}>
              Location:
            </Text>
            <Text style={{color: 'black', textAlign: 'center'}}>
              {targetAddress ? targetAddress : 'Empty Location'}
            </Text>

            {address ? (
              <TouchableOpacity
                style={styles.button}
                onPress={handleOpenGoogleMaps}>
                <Text style={[styles.buttonText, {textAlign: 'center'}]}>
                  Open in google maps
                </Text>
              </TouchableOpacity>
            ) : null}
          </View>
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  background: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  container: {
    ...StyleSheet.absoluteFillObject,
    flex: 1,
    marginTop: 50,
    alignItems: 'center',
    textAlign: 'center',
    padding: 30,
  },
  navigationBar: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
    backgroundColor: '#3A9106',
  },
  errorText: {
    color: 'red',
    fontSize: 16,
    fontWeight: 'bold',
  },
  button: {
    marginTop: 30,
    padding: 10,
    backgroundColor: '#3A9106',
    borderRadius: 5,
  },
  buttonText: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
  },
});

export default MapTracker;
