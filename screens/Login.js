import React, {useState} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  ImageBackground,
  Alert,
  Image,
} from 'react-native';
import {BACKEND_URL} from '@env';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

import {REVERB_APP_KEY, REVERB_HOST, REVERB_PORT, REVERB_SCHEME} from '@env';
import Echo from 'laravel-echo';

console.log('Echo configuration:');
console.log(REVERB_APP_KEY, REVERB_HOST, REVERB_PORT, REVERB_SCHEME);

import Pusher from 'pusher-js/react-native';
// window.Pusher = Pusher;

// window.Echo = new Echo({
//   broadcaster: "reverb",
//   key: REVERB_APP_KEY,
//   wsHost: REVERB_HOST,
//   wsPort: REVERB_PORT,
//   wssPort: REVERB_PORT,
//   forceTLS: (REVERB_SCHEME ?? "https") === "https",
//   enabledTransports: ["ws", "wss"],
// });

// setInterval(() => {
//   window.Echo.channel("location").listen("PinLocation", (e) => {
//     console.log('masu', e);
//   });
// }, 200);

// window.Echo = new Echo({
//   broadcaster: 'reverb',
//   key: REVERB_APP_KEY,
//   wsHost: REVERB_HOST,
//   wsPort: REVERB_PORT,
//   wssPort: REVERB_PORT,
//   forceTLS: (REVERB_SCHEME ?? 'https') === 'https',
//   enabledTransports: ['ws', 'wss'],
// });

Pusher.logToConsole = true;

let PusherClient = new Pusher(REVERB_APP_KEY, {
  cluster: 'mt1',
  wsHost: '192.168.2.109',
  wsPort: REVERB_PORT,
  enabledTransports: ['ws'],
  forceTLS: false,
});

let echo = new Echo({
  broadcaster: 'pusher',
  client: PusherClient,
});

console.log("echo", echo)

setInterval(() => {
  echo.channel('location').listen('PinLocation', e => {
    console.log('masuk', e);
  });
}, 200);

const Login = ({navigation}) => {
  const [phoneNumber, setPhoneNumber] = useState('');
  const [password, setPassword] = useState('');
  const [showPassword, setShowPassword] = useState(false);

  const togglePasswordVisibility = () => {
    setShowPassword(!showPassword);
  };

  const handleLogin = async () => {
    if (!phoneNumber || !password) {
      Alert.alert('Error', 'Phone and password should be fill');
      return;
    }

    try {
      const response = await axios.post(`${BACKEND_URL}/api/auth/login`, {
        phone: phoneNumber,
        password,
      });

      await AsyncStorage.setItem('user', JSON.stringify(response.data.result));
      navigation.navigate('Home');
    } catch (err) {
      console.log('err', err);
      Alert.alert('Error', err.response.data.error);
    }
  };

  return (
    <View>
      <ImageBackground
        style={styles.container_header}
        source={require('../assets/images/green.png')}>
        <View style={styles.header}>
          <Text style={styles.headerText}>Welcome to CSOW!</Text>
        </View>
      </ImageBackground>
      <View style={styles.background}>
        <View style={styles.container}>
          <View style={styles.formContainer}>
            <Text style={{color: 'black', marginBottom: 8}}>Phone Number</Text>
            <TextInput
              style={styles.input}
              placeholderTextColor="grey"
              placeholder="08xxxxxxxxx"
              value={phoneNumber}
              onChangeText={text => setPhoneNumber(text.replace(/[^0-9]/g, ''))}
              keyboardType="phone-pad"
              autoCompleteType="tel"
            />

            <Text style={{color: 'black', marginBottom: 8}}>Password</Text>
            <View style={styles.inputContainer}>
              <TextInput
                style={styles.input}
                placeholder="Fill your password"
                placeholderTextColor="grey"
                value={password}
                onChangeText={setPassword}
                secureTextEntry={!showPassword}
              />
              <TouchableOpacity
                onPress={togglePasswordVisibility}
                style={styles.eyeIcon}>
                <Image
                  style={{height: 20, width: 20}}
                  source={require('../assets/images/eye.png')}
                />
              </TouchableOpacity>
            </View>

            <TouchableOpacity onPress={() => navigation.navigate('FormPhone')}>
              <Text style={styles.forgotPasswordText}>Forgot password?</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.button} onPress={handleLogin}>
              <Text style={styles.buttonText}>Login</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => navigation.navigate('FirstSignUp')}>
              <View style={styles.container_daftar}>
                <Text style={{color: 'black'}}>You dont have account?</Text>
                <Text style={styles.signupText}>Register</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  inputContainer: {
    position: 'relative',
  },
  input: {
    width: '100%',
    height: 40,
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 5,
    paddingHorizontal: 10,
    marginBottom: 20,
    color: 'black',
  },
  eyeIcon: {
    position: 'absolute',
    top: 10, // adjust as needed
    right: 10, // adjust as needed
  },
  background: {
    padding: 20,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
  container_header: {
    height: 200,
    overflow: 'hidden',
    borderBottomLeftRadius: 50,
    borderBottomRightRadius: 50,
  },
  headerText: {
    color: 'white',
    fontSize: 25,
    fontWeight: 'bold',
  },
  header: {
    flex: 1,
    fontWeight: 'bold',
    marginTop: 50,
    marginLeft: 30,
  },
  container_daftar: {
    marginTop: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  signupText: {
    color: 'blue',
    marginLeft: 5,
  },
  formContainer: {
    justifyContent: 'center',
  },
  // input: {
  //   width: '100%',
  //   height: 40,
  //   borderWidth: 1,
  //   borderColor: '#ccc',
  //   borderRadius: 5,
  //   paddingHorizontal: 10,
  //   marginBottom: 20,
  //   color: 'black',
  // },
  button: {
    width: '100%',
    marginTop: 220,
    borderRadius: 10,
    height: 50,
    backgroundColor: '#3A9106',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
  },
  forgotPasswordText: {
    color: '#007bff',
    fontSize: 14,
    marginBottom: 15,
    textAlign: 'right',
  },
  imageOverlay: {
    ...StyleSheet.absoluteFillObject,
  },
});

export default Login;
