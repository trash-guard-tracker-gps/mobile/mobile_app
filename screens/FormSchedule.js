import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Image,
  Alert,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
} from 'react-native';
import {useIsFocused, useNavigation} from '@react-navigation/native';
import DateTimePicker from '@react-native-community/datetimepicker';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Geolocation from '@react-native-community/geolocation';
import {format} from 'date-fns-tz';
import {BACKEND_URL} from '@env';

const FormSchedule = () => {
  const navigation = useNavigation();
  const isFocused = useIsFocused();
  const [name, setName] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');
  const [address, setAddress] = useState('');
  const [pickupDate, setPickupDate] = useState(new Date());
  const [pickupTime, setPickupTime] = useState(new Date());
  const [showDatePicker, setShowDatePicker] = useState(false);
  const [showTimePicker, setShowTimePicker] = useState(false);
  const [additionalInfo, setAdditionalInfo] = useState('');

  const [user, setUser] = useState(null);
  const [statusButton, setStatusButton] = useState(false);

  useEffect(() => {
    (async () => {
      try {
        const userData = await AsyncStorage.getItem('user');
        if (userData !== null) {
          const parsedUserData = JSON.parse(userData);
          setUser(parsedUserData);
        }
      } catch (err) {
        console.log('Error retrieving user data:', err);
        Alert.alert('Error', err.response.data.error);
      }
    })();
  }, [statusButton, isFocused]);

  const handlePickup = async () => {
    try {
      const pickupDateTime = new Date(
        pickupDate.getFullYear(),
        pickupDate.getMonth(),
        pickupDate.getDate(),
        pickupTime.getHours(),
        pickupTime.getMinutes(),
        pickupTime.getSeconds(),
      );

      const pickupDateTimeIndonesia = format(
        pickupDateTime,
        'yyyy-MM-dd HH:mm:ss',
        {timeZone: 'Asia/Jakarta'},
      );

      getCurrentLocation();

      const response = await axios.post(`${BACKEND_URL}/api/order/create`, {
        customer_id: user.id,
        name,
        phone: phoneNumber,
        address,
        pick_up: pickupDateTimeIndonesia,
        add_information: additionalInfo,
      });

      console.log('Response:', response.data);

      const userData = await AsyncStorage.getItem('user');
      if (userData !== null) {
        const currentUserData = JSON.parse(userData);
        const updatedUserData = {
          ...currentUserData,
          status: '0' || currentUserData.status,
          orderId: response.data.result.id,
        };
        await AsyncStorage.setItem('user', JSON.stringify(updatedUserData));
      }

      navigation.navigate('Home', {refresh: true});
    } catch (err) {
      console.log('Error:', err);
      Alert.alert('Error', err.response.data.error);
    } finally {
      setStatusButton(!statusButton);
    }
  };

  const getCurrentLocation = () => {
    Geolocation.getCurrentPosition(
      position => {
        const {latitude, longitude} = position.coords;
        updateLocation(latitude, longitude);
      },
      error => {
        console.error('Error getting current location:', error);
      },
      {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000},
    );
  };

  const updateLocation = async (lat, long) => {
    try {
      await axios.post(`${BACKEND_URL}/api/customer/update/${user.id}`, {
        lat,
        long,
      });
      console.log('Location updated successfully');
    } catch (error) {
      console.error('Error updating location:', error);
    }
  };

  const onChangeDate = (event, selectedDate) => {
    const currentDate = selectedDate || pickupDate;
    setShowDatePicker(false);
    setPickupDate(currentDate);
  };

  const onChangeTime = (event, selectedTime) => {
    const currentTime = selectedTime || pickupTime;
    setShowTimePicker(false);
    setPickupTime(currentTime);
  };

  const formatDateIndonesian = date => {
    return format(date, 'dd/MM/yyyy', {timeZone: 'Asia/Jakarta'});
  };

  const formatTimeIndonesian = date => {
    return format(date, 'HH:mm', {timeZone: 'Asia/Jakarta'});
  };

  return (
    <KeyboardAvoidingView
      style={styles.background}
      behavior={Platform.OS === 'ios' ? 'padding' : null}
      keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}>
      <ScrollView contentContainerStyle={styles.scrollContainer}>
        <View style={styles.navigationBar}>
          <TouchableOpacity
            style={{marginLeft: 10}}
            onPress={() => navigation.goBack()}>
            <Image source={require('../assets/images/arrow.png')} />
          </TouchableOpacity>
          <Text style={styles.headerText}>Pickup information</Text>
        </View>
        <View style={styles.container}>
          <Text style={styles.sectionHeader}>Pickup Detail</Text>

          <View style={styles.formContainer}>
            <Text style={styles.label}>Name</Text>
            <TextInput
              style={styles.input}
              placeholder="Fill your name"
              placeholderTextColor="grey"
              value={name}
              onChangeText={setName}
            />

            <Text style={styles.label}>Phone Number</Text>
            <TextInput
              style={styles.input}
              placeholder="Fill your phone number"
              placeholderTextColor="grey"
              value={phoneNumber}
              onChangeText={text => setPhoneNumber(text.replace(/[^0-9]/g, ''))}
              keyboardType="phone-pad"
              autoCompleteType="tel"
            />

            <Text style={styles.label}>Address</Text>
            <TextInput
              style={[styles.input, {height: 100}]}
              placeholder="Fill your address"
              placeholderTextColor="grey"
              value={address}
              onChangeText={setAddress}
              multiline={true}
            />
          </View>

          <Text style={styles.sectionHeader}>Pickup schedule</Text>
          <View style={styles.formContainer}>
            <TouchableOpacity
              style={styles.datePickerButton}
              onPress={() => setShowDatePicker(true)}>
              <Text style={styles.datePickerText}>
                Determine the pickup schedule:{' '}
                {formatDateIndonesian(pickupDate)}
              </Text>
            </TouchableOpacity>

            {showDatePicker && (
              <DateTimePicker
                style={styles.input}
                testID="datePicker"
                value={pickupDate}
                placeholderTextColor="grey"
                mode="date"
                is24Hour={true}
                display="default"
                onChange={onChangeDate}
              />
            )}
            <TouchableOpacity
              style={styles.datePickerButton}
              onPress={() => setShowTimePicker(true)}>
              <Text style={styles.datePickerText}>
                Determine the pickup time: {formatTimeIndonesian(pickupTime)}
              </Text>
            </TouchableOpacity>

            {showTimePicker && (
              <DateTimePicker
                style={styles.input}
                testID="timePicker"
                value={pickupTime}
                placeholderTextColor="grey"
                mode="time"
                is24Hour={true}
                display="default"
                onChange={onChangeTime}
              />
            )}

            <Text style={styles.label}>Additional information</Text>
            <TextInput
              style={[styles.input, {height: 100}]}
              placeholder="Fill your additional information"
              placeholderTextColor="grey"
              value={additionalInfo}
              onChangeText={setAdditionalInfo}
              multiline={true}
            />
            <TouchableOpacity style={styles.button} onPress={handlePickup}>
              <Text style={styles.buttonText}>Create</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  background: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  scrollContainer: {
    flexGrow: 1,
  },
  container: {
    flex: 1,
    backgroundColor: 'rgba(255, 255, 255, 0.8)',
    padding: 20,
  },
  navigationBar: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
    backgroundColor: '#3A9106',
  },
  headerText: {
    color: 'white',
    fontWeight: 'bold',
    marginLeft: 10,
    flex: 1, // to fill remaining space
  },
  sectionHeader: {
    marginBottom: 5,
    color: 'black',
    fontSize: 15,
    fontWeight: 'bold',
  },
  formContainer: {
    justifyContent: 'center',
  },
  input: {
    width: '100%',
    height: 40,
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 5,
    paddingHorizontal: 10,
    marginBottom: 20,
    color: 'black',
  },
  label: {
    color: 'black',
    marginBottom: 10,
  },
  datePickerButton: {
    width: '100%',
    height: 40,
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 5,
    paddingHorizontal: 10,
    marginBottom: 20,
    justifyContent: 'center',
  },
  datePickerText: {
    color: 'grey',
  },
  button: {
    width: '100%',
    marginTop: 2,
    borderRadius: 10,
    height: 50,
    backgroundColor: '#3A9106',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
  },
});

export default FormSchedule;
