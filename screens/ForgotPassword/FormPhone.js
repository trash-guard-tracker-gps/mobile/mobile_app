import React, {useState} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Image,
  ActivityIndicator,
  Alert,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {BACKEND_URL} from '@env';
import axios from 'axios';

const FormPhone = () => {
  const navigation = useNavigation();
  const [phoneNumber, setPhoneNumber] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  const handleFormPhone = async () => {
    // navigation.navigate('FormSendOtp');
    try {
      if (!phoneNumber.trim()) {
        Alert.alert('Error', 'Please enter a valid phone number');
        return;
      }
      setIsLoading(true);
      const response = await axios.post(
        `${BACKEND_URL}/api/auth/forgot-password`,
        {
          phone: phoneNumber,
        },
      );
      setPhoneNumber('');
      navigation.navigate('FormSendOtp', {
        id: response.data.result ? response.data.result.id : null,
      });
    } catch (err) {
      console.log('Error:', err);
      Alert.alert('Error', err.response.data.error);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <View style={styles.background}>
      <View style={styles.navigationBar}>
        <TouchableOpacity
          style={{marginLeft: 5}}
          onPress={() => navigation.goBack()}>
          <Image source={require('../../assets/images/arrow.png')} />
        </TouchableOpacity>
        <Text style={{color: 'white', fontWeight: 'bold', marginLeft: 10}}>
          Forgot password
        </Text>
      </View>
      <View style={styles.container}>
        <Text
          style={{
            textAlign: 'center',
            marginBottom: 20,
            color: 'black',
            fontSize: 15,
            fontWeight: 'bold',
            marginTop: 20,
          }}>
          Please enter your phone number to reset your password
        </Text>

        <View style={styles.formContainer}>
          <Text style={{color: 'black', marginBottom: 10, marginTop: 20}}>
            Phone Number
          </Text>
          <TextInput
            style={styles.input}
            placeholder="Fill your phone number"
            placeholderTextColor="grey"
            value={phoneNumber}
            onChangeText={text => setPhoneNumber(text.replace(/[^0-9]/g, ''))}
            keyboardType="phone-pad"
            autoCompleteType="tel"
          />
          {isLoading ? (
            <ActivityIndicator style={styles.button} color="#fff" />
          ) : (
            <TouchableOpacity
              style={[styles.button, isLoading && {opacity: 0.5}]}
              onPress={handleFormPhone}
              disabled={isLoading}>
              <Text style={styles.buttonText}>Next</Text>
            </TouchableOpacity>
          )}
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  background: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  container: {
    flex: 1,
    backgroundColor: 'rgba(255, 255, 255, 0.8)',
    padding: 20,
  },
  navigationBar: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
    backgroundColor: '#3A9106',
  },
  headerText: {
    fontSize: 24,
    fontWeight: 'bold',
  },
  formContainer: {
    justifyContent: 'center',
  },
  input: {
    width: '100%',
    height: 50,
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 5,
    paddingHorizontal: 10,
    marginBottom: 20,
    color: 'black',
  },
  button: {
    width: '100%',
    marginTop: 420,
    borderRadius: 10,
    height: 50,
    backgroundColor: '#3A9106',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
  },
});

export default FormPhone;
