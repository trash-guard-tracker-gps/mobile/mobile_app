import React, {useState} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Image,
  Alert,
} from 'react-native';
import {useNavigation, useRoute} from '@react-navigation/native';
import {BACKEND_URL} from '@env';
import axios from 'axios';

const FormSendOtp = () => {
  const navigation = useNavigation();
  const route = useRoute();
  const [otp, setOtp] = useState(['', '', '', '']);

  const handleVerifyOtp = async () => {
    // navigation.navigate('FormChangePassword', {id: customerId});

    const customerId = route.params.id || null;

    try {
      const otpValue = otp.join('');

      await axios.post(`${BACKEND_URL}/api/auth/verify-otp/` + customerId, {
        otp: otpValue,
      });

      setOtp(['', '', '', '']);
      navigation.navigate('FormChangePassword', {id: customerId});
    } catch (err) {
      console.error('Error:', err);
      Alert.alert('Error', err.response.data.error);
    }
  };

  const handleChangeText = (text, index) => {
    if (/^[0-9]*$/.test(text)) {
      const newOtp = [...otp];
      newOtp[index] = text;
      setOtp(newOtp);
    }
  };

  return (
    <View style={styles.background}>
      <View style={styles.navigationBar}>
        <TouchableOpacity
          style={{marginLeft: 10}}
          onPress={() => navigation.goBack()}>
          <Image source={require('../../assets/images/arrow.png')} />
        </TouchableOpacity>
        <Text style={{color: 'white', fontWeight: 'bold', marginLeft: 10}}>
          OTP
        </Text>
      </View>
      <View style={styles.container}>
        <Text
          style={{
            textAlign: 'center',
            marginBottom: 20,
            color: 'black',
            fontSize: 15,
            fontWeight: 'bold',
            marginTop: 20,
          }}>
          An OTP code has been sent to your telephone number
        </Text>
        <View style={styles.formContainer}>
          <View style={styles.otpContainer}>
            {[0, 1, 2, 3].map(index => (
              <TextInput
                key={index}
                style={styles.otpInput}
                placeholder="-"
                placeholderTextColor="grey"
                maxLength={1}
                value={otp[index]}
                onChangeText={text => handleChangeText(text, index)}
                keyboardType="numeric"
              />
            ))}
          </View>
          {/* <Text style={styles.timerText}>Time remaining: {timer} seconds</Text> */}
          <TouchableOpacity style={styles.button} onPress={handleVerifyOtp}>
            <Text style={styles.buttonText}>Next</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  background: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  container: {
    flex: 1,
    backgroundColor: 'rgba(255, 255, 255, 0.8)',
    padding: 20,
  },
  navigationBar: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
    backgroundColor: '#3A9106',
  },
  formContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  otpContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '60%',
    marginTop: 20,
  },
  otpInput: {
    width: '20%',
    height: 40,
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 5,
    paddingHorizontal: 10,
    textAlign: 'center',
    color: 'black',
  },
  timerText: {
    marginBottom: 35,
  },
  button: {
    width: '100%',
    marginTop: 500,
    borderRadius: 10,
    height: 50,
    backgroundColor: '#3A9106',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
  },
});

export default FormSendOtp;
