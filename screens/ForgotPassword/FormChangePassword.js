import React, {useState} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Image,
  Alert,
} from 'react-native';
import {useNavigation, useRoute} from '@react-navigation/native';
import {BACKEND_URL} from '@env';
import axios from 'axios';

const FormChangePassword = () => {
  const navigation = useNavigation();
  const route = useRoute();
  const [newPassword, setNewPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [showPassword, setShowPassword] = useState(false);
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);

  const handleChangePassword = async () => {
    const customerId = route.params?.id;

    if (newPassword !== confirmPassword) {
      Alert.alert('Error', 'New password and confirm password should be same');
      return;
    }

    try {
      await axios.post(`${BACKEND_URL}/api/customer/update/${customerId}`, {
        password: newPassword,
        otp: 0,
      });

      setNewPassword('');
      setConfirmPassword('');

      navigation.navigate('Login');
    } catch (err) {
      Alert.alert('Error', err.response.data.error);
    }
  };

  return (
    <View style={styles.background}>
      <View style={styles.navigationBar}>
        <TouchableOpacity
          style={{marginLeft: 10}}
          onPress={() => navigation.goBack()}>
          <Image source={require('../../assets/images/arrow.png')} />
        </TouchableOpacity>
        <Text style={{color: 'white', fontWeight: 'bold', marginLeft: 10}}>
          Change password
        </Text>
      </View>
      <View style={styles.container}>
        <Text
          style={{
            textAlign: 'center',
            marginTop: 25,
            marginBottom: 40,
            color: 'black',
            fontSize: 15,
            fontWeight: 'bold',
          }}>
          Please enter your new password and confirm password
        </Text>
        <View style={styles.formContainer}>
          <Text style={{color: 'black', marginBottom: 10}}>New Password</Text>
          <View style={styles.passwordInputContainer}>
            <TextInput
              style={styles.input}
              placeholder="Fill your new password"
              placeholderTextColor="grey"
              value={newPassword}
              onChangeText={setNewPassword}
              secureTextEntry={!showPassword}
            />
            <TouchableOpacity
              style={styles.eyeIconContainer}
              onPress={() => setShowPassword(!showPassword)}>
              <Image
                source={require('../../assets/images/eye.png')}
                style={styles.eyeIcon}
              />
            </TouchableOpacity>
          </View>
          <Text style={{color: 'black', marginBottom: 10}}>
            Confirm Password
          </Text>
          <View style={styles.passwordInputContainer}>
            <TextInput
              style={styles.input}
              placeholder="Fill your confirm password"
              placeholderTextColor="grey"
              value={confirmPassword}
              onChangeText={setConfirmPassword}
              secureTextEntry={!showConfirmPassword}
            />
            <TouchableOpacity
              style={styles.eyeIconContainer}
              onPress={() => setShowConfirmPassword(!showConfirmPassword)}>
              <Image
                source={require('../../assets/images/eye.png')}
                style={styles.eyeIcon}
              />
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            style={styles.button}
            onPress={handleChangePassword}>
            <Text style={styles.buttonText}>Change Password</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  background: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  container: {
    flex: 1,
    backgroundColor: 'rgba(255, 255, 255, 0.8)',
    padding: 20,
  },
  navigationBar: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
    backgroundColor: '#3A9106',
  },
  formContainer: {
    justifyContent: 'center',
  },
  input: {
    flex: 1,
    height: 40,
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 5,
    paddingHorizontal: 10,
    color: 'black',
  },
  passwordInputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    position: 'relative',
    marginBottom: 20,
  },
  eyeIconContainer: {
    position: 'absolute',
    right: 10,
    zIndex: 1,
  },
  eyeIcon: {
    height: 20,
    width: 20,
  },
  button: {
    width: '100%',
    borderRadius: 10,
    height: 50,
    backgroundColor: '#3A9106',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
  },
});

export default FormChangePassword;
