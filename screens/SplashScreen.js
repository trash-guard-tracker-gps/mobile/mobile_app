import React, {useEffect} from 'react';
import {View, StyleSheet, Image} from 'react-native';

const SplashScreen = ({navigation}) => {
  useEffect(() => {
    // Simulate a delay while the splash screen is displayed (e.g., 2 seconds)
    const timer = setTimeout(() => {
      // Navigate to the Login screen after the delay
      navigation.replace('Login');
    }, 2000);

    // Clean up the timer to avoid memory leaks
    return () => clearTimeout(timer);
  }, [navigation]);

  return (
    <View style={styles.container}>
      <Image
        source={require('../assets/images/logo.png')}
        style={styles.logo}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  logo: {
    width: 300,
    height: 110,
  },
});

export default SplashScreen;
