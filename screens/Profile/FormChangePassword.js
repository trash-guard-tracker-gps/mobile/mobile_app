import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Image,
  Alert,
  KeyboardAvoidingView,
  ScrollView,
  Platform,
} from 'react-native';
import {useIsFocused, useNavigation} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {BACKEND_URL} from '@env';
import axios from 'axios';

const FormChangePassword = () => {
  const navigation = useNavigation();
  const isFocused = useIsFocused();
  const [id, setId] = useState(0);
  const [newPassword, setNewPassword] = useState('');
  const [confirmNewPassword, setConfirmNewPassword] = useState('');
  const [statusButton, setStatusButton] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);

  useEffect(() => {
    (async () => {
      try {
        const userData = await AsyncStorage.getItem('user');
        if (userData !== null) {
          const parsedUserData = JSON.parse(userData);
          setId(parsedUserData.id);
        }
      } catch (err) {
        console.log('Error retrieving user data:', err);
        Alert.alert('Error', err.response.data.error);
      }
    })();
  }, [statusButton, isFocused]);

  const handleChangePassword = async () => {
    if (newPassword !== confirmNewPassword) {
      Alert.alert('Error', 'Password does not match the confirm password');
      return;
    }

    try {
      const response = await axios.post(
        `${BACKEND_URL}/api/customer/update/${id}`,
        {
          password: newPassword,
        },
      );

      console.log('Update successful', response);

      const userData = await AsyncStorage.getItem('user');
      if (userData !== null) {
        const currentUserData = JSON.parse(userData);
        const updatedUserData = {
          ...currentUserData,
        };
        await AsyncStorage.setItem('user', JSON.stringify(updatedUserData));
      }

      navigation.navigate('Detail');
    } catch (err) {
      console.log('Error updating password:', err);
      Alert.alert('Error', err.response.data.error);
    } finally {
      setStatusButton(!statusButton);
    }
  };

  return (
    <KeyboardAvoidingView
      style={styles.background}
      behavior={Platform.OS === 'ios' ? 'padding' : null}>
      <ScrollView contentContainerStyle={styles.container}>
        <View style={styles.navigationBar}>
          <TouchableOpacity
            style={{marginLeft: 10}}
            onPress={() => navigation.goBack()}>
            <Image source={require('../../assets/images/arrow.png')} />
          </TouchableOpacity>
          <Text style={{color: 'white', fontWeight: 'bold', marginLeft: 10}}>
            Change password
          </Text>
        </View>
        <View style={styles.content}>
          <Text
            style={{
              textAlign: 'center',
              marginTop: 10,
              marginBottom: 20,
              color: 'black',
              fontSize: 15,
              fontWeight: 'bold',
            }}>
            Changing password
          </Text>

          <View style={styles.formContainer}>
            <Text style={{color: 'black', marginBottom: 10}}>New password</Text>
            <View style={styles.passwordInputContainer}>
              <TextInput
                style={styles.input}
                placeholder="Fill your new password"
                placeholderTextColor="grey"
                value={newPassword}
                onChangeText={setNewPassword}
                secureTextEntry={!showPassword}
              />
              <TouchableOpacity
                style={styles.eyeIconContainer}
                onPress={() => setShowPassword(!showPassword)}>
                <Image
                  source={require('../../assets/images/eye.png')}
                  style={styles.eyeIcon}
                />
              </TouchableOpacity>
            </View>
            <Text style={{color: 'black', marginBottom: 10}}>
              Confirm new password
            </Text>
            <View style={styles.passwordInputContainer}>
              <TextInput
                style={styles.input}
                placeholder="Fill your new confirm password"
                placeholderTextColor="grey"
                value={confirmNewPassword}
                onChangeText={setConfirmNewPassword}
                secureTextEntry={!showConfirmPassword}
              />
              <TouchableOpacity
                style={styles.eyeIconContainer}
                onPress={() => setShowConfirmPassword(!showConfirmPassword)}>
                <Image
                  source={require('../../assets/images/eye.png')}
                  style={styles.eyeIcon}
                />
              </TouchableOpacity>
            </View>

            <TouchableOpacity
              style={styles.button}
              onPress={handleChangePassword}>
              <Text style={styles.buttonText}>Save</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  background: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  container: {
    flexGrow: 1,
    backgroundColor: 'rgba(255, 255, 255, 0.8)',
    paddingBottom: 20,
  },
  navigationBar: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
    backgroundColor: '#3A9106',
  },
  content: {
    flex: 1,
    padding: 20,
  },
  formContainer: {
    justifyContent: 'center',
  },
  input: {
    flex: 1,
    height: 40,
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 5,
    paddingHorizontal: 10,
    color: 'black',
  },
  passwordInputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10, // Added marginBottom to create space between input and eye icon
  },
  eyeIconContainer: {
    position: 'absolute',
    right: 10,
  },
  eyeIcon: {
    height: 20,
    width: 20,
  },
  button: {
    width: '100%',
    borderRadius: 10,
    height: 50,
    backgroundColor: '#3A9106',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 400,
  },
  buttonText: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
  },
});

export default FormChangePassword;
