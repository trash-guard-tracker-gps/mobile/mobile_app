import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Image,
  Alert,
  KeyboardAvoidingView,
  ScrollView,
  Platform,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {BACKEND_URL} from '@env';
import axios from 'axios';

const FormChangeInformation = () => {
  const navigation = useNavigation();
  const [id, setId] = useState(0);
  const [name, setName] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');
  const [email, setEmail] = useState('');
  const [address, setAddress] = useState('');
  const [statusButton, setStatusButton] = useState(false);

  useEffect(() => {
    (async () => {
      try {
        const userData = await AsyncStorage.getItem('user');
        if (userData !== null) {
          const parsedUserData = JSON.parse(userData);
          setId(parsedUserData.id);
          setName(parsedUserData.name);
          setEmail(parsedUserData.email);
          setPhoneNumber(parsedUserData.phone);
          setAddress(parsedUserData.address);
        }
      } catch (err) {
        console.log('Error retrieving user data:', err);
        Alert.alert('Error', err.response.data.error);
      }
    })();
  }, []);

  const handleChangeInformation = async () => {
    const emailRegex = /\S+@\S+\.\S+/;

    if (!emailRegex.test(email)) {
      Alert.alert('Error', 'Please enter a valid email address with use @');
      return;
    }

    if (!name || !email || !phoneNumber || !address) {
      Alert.alert(
        'Error',
        'Please dont make input name, email, phone, address empty',
      );
      return;
    }

    try {
      await axios.post(`${BACKEND_URL}/api/customer/update/${id}`, {
        name,
        email,
        phone: phoneNumber,
        address,
      });

      const userData = await AsyncStorage.getItem('user');
      if (userData !== null) {
        const currentUserData = JSON.parse(userData);
        const updatedUserData = {
          ...currentUserData,
          name: name || currentUserData.name, // Update name if provided, otherwise keep the current name
          email: email || currentUserData.email, // Update email if provided, otherwise keep the current email
          phone: phoneNumber || currentUserData.phone, // Update phone if provided, otherwise keep the current phone
          address: address || currentUserData.address, // Update address if provided, otherwise keep the current address
        };
        await AsyncStorage.setItem('user', JSON.stringify(updatedUserData));
      }

      navigation.navigate('Detail');
    } catch (err) {
      console.log('Error updating information:', err);
      Alert.alert('Error', err.response.data.error);
    } finally {
      setStatusButton(!statusButton);
    }
  };

  return (
    <KeyboardAvoidingView
      style={styles.background}
      behavior={Platform.OS === 'ios' ? 'padding' : null}
      keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}>
      <View style={styles.navigationBar}>
        <TouchableOpacity
          style={{marginLeft: 10}}
          onPress={() => navigation.goBack()}>
          <Image source={require('../../assets/images/arrow.png')} />
        </TouchableOpacity>
        <Text style={styles.headerText}>Change personal information</Text>
      </View>
      <ScrollView>
        <View style={styles.container}>
          <Text
            style={{
              textAlign: 'center',
              marginTop: 10,
              marginBottom: 20,
              color: 'black',
              fontSize: 15,
              fontWeight: 'bold',
            }}>
            Personal information
          </Text>

          <View style={styles.formContainer}>
            <Text style={{color: 'black', marginBottom: 10}}>Name</Text>
            <TextInput
              style={styles.input}
              placeholder="Name"
              placeholderTextColor="grey"
              value={name}
              onChangeText={setName}
            />
            <Text style={{color: 'black', marginBottom: 10}}>Email</Text>
            <TextInput
              style={styles.input}
              placeholder="Email"
              placeholderTextColor="grey"
              value={email}
              onChangeText={setEmail}
            />
            <Text style={{color: 'black', marginBottom: 10}}>Phone Number</Text>
            <TextInput
              style={styles.input}
              placeholder="No handphone"
              placeholderTextColor="grey"
              value={phoneNumber}
              onChangeText={text => setPhoneNumber(text.replace(/[^0-9]/g, ''))}
              keyboardType="phone-pad"
              autoCompleteType="tel"
            />
            <Text style={{color: 'black', marginBottom: 10}}>Address</Text>
            <TextInput
              style={[styles.input, {height: 150}]}
              placeholder="Alamat"
              placeholderTextColor="grey"
              value={address}
              onChangeText={setAddress}
            />

            <TouchableOpacity
              style={styles.button}
              onPress={handleChangeInformation}>
              <Text style={styles.buttonText}>Save</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  background: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  container: {
    flex: 1,
    backgroundColor: 'rgba(255, 255, 255, 0.8)',
    padding: 20,
  },
  navigationBar: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
    backgroundColor: '#3A9106',
  },
  headerText: {
    color: 'white',
    fontWeight: 'bold',
    marginLeft: 10,
  },
  formContainer: {
    justifyContent: 'center',
  },
  input: {
    width: '100%',
    height: 40,
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 5,
    paddingHorizontal: 10,
    marginBottom: 20,
    color: 'black',
  },
  button: {
    width: '100%',
    marginTop: 100,
    borderRadius: 10,
    height: 50,
    backgroundColor: '#3A9106',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
  },
});

export default FormChangeInformation;
