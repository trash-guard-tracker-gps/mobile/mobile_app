import AsyncStorage from '@react-native-async-storage/async-storage';
import {useIsFocused, useNavigation} from '@react-navigation/native';
import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  ImageBackground,
  Image,
  Alert,
} from 'react-native';

const Detail = () => {
  const navigation = useNavigation();
  const isFocused = useIsFocused();
  const [status, setStatus] = useState('');
  const [name, setName] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');

  useEffect(() => {
    (async () => {
      try {
        const userData = await AsyncStorage.getItem('user');
        if (userData !== null) {
          const parsedUserData = JSON.parse(userData);
          setStatus(parsedUserData.status);
          setName(parsedUserData.name);
          setPhoneNumber(parsedUserData.phone);
        }
      } catch (err) {
        console.error('Error retrieving user data:', err);
      }
    })();
  }, [isFocused]);

  const handleLogout = async () => {
    try {
      await AsyncStorage.clear();
      await AsyncStorage.removeItem('user');
      navigation.navigate('Login');
    } catch (err) {
      console.log('Error logging out:', err);
    }
  };

  const confirmLogout = () => {
    Alert.alert(
      'Logout',
      'Are you sure you want to logout?',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Logout cancelled'),
          style: 'cancel',
        },
        {
          text: 'Yes',
          onPress: handleLogout,
        },
      ],
      {cancelable: false},
    );
  };

  return (
    <View style={styles.background}>
      <ImageBackground
        style={styles.container_header}
        source={require('../../assets/images/green.png')}>
        <View style={styles.header}>
          <Text
            style={[styles.headerText, {color: 'white', textAlign: 'center'}]}>
            {name ? name.charAt(0).toUpperCase() + name.slice(1) : ''}
          </Text>
          <Text
            style={{
              color: 'white',
              textAlign: 'center',
              marginTop: 10,
              fontWeight: 'bold',
            }}>
            {phoneNumber}
          </Text>
        </View>
      </ImageBackground>
      <View style={styles.container}>
        <View style={styles.bodyContainer}>
          <TouchableOpacity
            onPress={() => navigation.navigate('FormChangeInformation')}>
            <Text style={styles.requestTextLeft}>Personal information</Text>
          </TouchableOpacity>
          <View style={styles.hr}></View>
          <TouchableOpacity
            onPress={() => navigation.navigate('FormChangePasswordProfile')}>
            <Text style={styles.requestTextLeft}>Password</Text>
          </TouchableOpacity>
          <View style={styles.hr}></View>
          <TouchableOpacity onPress={confirmLogout}>
            <Text style={[styles.requestTextLeft, {color: 'red'}]}>Logout</Text>
          </TouchableOpacity>
          <View style={styles.hr}></View>
        </View>
      </View>

      <View style={styles.bottomBar}>
        <TouchableOpacity
          onPress={() => navigation.navigate('Home', {refresh: true})}
          style={[styles.barItem]}>
          <Image
            style={{color: 'white', padding: 1}}
            source={require('../../assets/images/home.png')}
          />
          <Text style={{color: 'grey'}}>Home</Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            if (status == 5 || status == 4) {
              navigation.navigate('FormSchedule', {refresh: true});
            }
          }}
          style={styles.barItem}>
          <Image
            style={{color: 'white', padding: 1}}
            source={require('../../assets/images/truck.png')}
          />
          <Text style={{color: 'grey'}}>Pickup Menu</Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            navigation.navigate('MapTracker');
          }}
          style={styles.barItem}>
          <Image
            style={{color: 'white', padding: 1}}
            source={require('../../assets/images/location.png')}
          />
          <Text style={{color: 'grey'}}>Track</Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => navigation.navigate('Detail', {refresh: true})}
          style={styles.barItem}>
          <Image
            style={{
              color: 'white',
              padding: 1,
              tintColor: '#3A9106',
            }}
            source={require('../../assets/images/profile.png')}
          />
          <Text style={{color: '#3A9106'}}>Account</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  background: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'space-between',
  },
  container: {
    flex: 1,
    backgroundColor: 'rgba(255, 255, 255, 0.8)',
    padding: 20,
  },
  container_header: {
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    height: 120,
    borderBottomLeftRadius: 50,
    borderBottomRightRadius: 50,
    overflow: 'hidden',
  },
  header: {
    alignItems: 'center',
    marginBottom: 0,
    marginTop: 0,
    padding: 20,
    backgroundColor: '#3A9106',
    borderRadius: 10,
  },
  headerText: {
    fontSize: 24,
    fontWeight: 'bold',
  },
  bodyContainer: {
    justifyContent: 'left',
    alignItems: 'left',
  },
  buttonsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '80%',
    marginBottom: 10,
  },
  button: {
    flex: 1,
    height: 40,
    backgroundColor: '#3A9106',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    margin: 5,
  },
  hr: {
    borderBottomColor: 'black',
    borderBottomWidth: 1,
    width: '100%',
    marginBottom: 10,
  },
  requestContainer: {
    width: '80%',
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    borderWidth: 1,
    marginBottom: 10,
  },
  requestTextLeft: {
    color: 'black',
    fontWeight: 'bold',
    marginBottom: 10,
    paddingBottom: 10,
    fontSize: 16,
    textAlign: 'left',
  },
  pickUpContainer: {
    backgroundColor: 'lightgreen',
    padding: 10,
    marginBottom: 20,
    width: '80%',
    borderRadius: 5,
  },
  greenText: {
    color: 'green',
    fontWeight: 'bold',
  },
  darkGreenText: {
    color: 'darkgreen',
  },
  bottomBar: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    padding: 20,
  },
  barItem: {
    alignItems: 'center',
  },
});

export default Detail;
