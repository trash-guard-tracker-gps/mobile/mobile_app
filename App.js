import React, {useEffect} from 'react';
import SplashScreen from './screens/SplashScreen';
import Login from './screens/Login';
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
import FormPhone from './screens/ForgotPassword/FormPhone';
import FormSendOtpForgotPassword from './screens/ForgotPassword/FormSendOtp';
import FormChangePassword from './screens/ForgotPassword/FormChangePassword';
import FirstSignUp from './screens/SignUp/FirstSignUp';
import FormSignUp from './screens/SignUp/FormSignUp';
import Home from './screens/Home';
import FormSchedule from './screens/FormSchedule';
import FormChangeInformation from './screens/Profile/FormChangeInformation';
import FormChangePasswordProfile from './screens/Profile/FormChangePassword';
import MapTracker from './screens/MapTracker';
import Detail from './screens/Profile/Detail';

const Stack = createStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{headerShown: false}}
        initialRouteName="Splash">
        <Stack.Screen name="Splash">
          {props => <SplashScreen {...props} />}
        </Stack.Screen>
        <Stack.Screen name="Login">
          {props => <Login {...props} />}
        </Stack.Screen>
        <Stack.Screen name="FormPhone">{() => <FormPhone />}</Stack.Screen>
        <Stack.Screen name="FormSendOtp">
          {() => <FormSendOtpForgotPassword />}
        </Stack.Screen>
        <Stack.Screen name="FormChangePassword">
          {() => <FormChangePassword />}
        </Stack.Screen>
        <Stack.Screen name="FirstSignUp">{() => <FirstSignUp />}</Stack.Screen>
        <Stack.Screen name="FormSignUp">{() => <FormSignUp />}</Stack.Screen>
        <Stack.Screen name="Home">{() => <Home />}</Stack.Screen>
        <Stack.Screen name="MapTracker">{() => <MapTracker />}</Stack.Screen>
        <Stack.Screen name="Detail">{() => <Detail />}</Stack.Screen>
        <Stack.Screen name="FormSchedule">
          {() => <FormSchedule />}
        </Stack.Screen>
        <Stack.Screen name="FormChangeInformation">
          {() => <FormChangeInformation />}
        </Stack.Screen>
        <Stack.Screen name="FormChangePasswordProfile">
          {() => <FormChangePasswordProfile />}
        </Stack.Screen>
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
