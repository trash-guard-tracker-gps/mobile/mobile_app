export const PickUpOrderStatus = {
  Process: 0,
  Approved: 1,
  OnTheWay: 2,
  Arrived: 3,
  None: 4,
};
